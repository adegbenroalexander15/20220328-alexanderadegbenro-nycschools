import Foundation

struct Constants {
   
    static let hsCellIdentifier = "nycHSCell"
    static let SATScoreSegue = "SATScoreSegue"
    static let highSchoolsURL = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    static let SATScoresURL = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
}

struct DetailConstants {
    struct Cells {
        
        static let schoolWithSATScoreCellIdentifier =  "HSSATScoresTableViewCell"
        static let schoolOverviewCellIdentifier = "HSOverViewTableViewCell"
        static let schoolWithAddressCellIdentifier = "AddressTableViewCell"
        static let schoolWithContactCellIdentifier = "HSContactTableViewCell"
    }
    
    static let averageSATMathScore = "SAT Average Math Score:   "
    static let averageSATWritingScore = "SAT Average Writing Score:   "
    static let averageSATReadingScore = "SAT Average Critical Reading Score:  "
    static let noSATScoreInfomationText = "There is no SAT score information for this high school"
}
