import UIKit

class HSSatScoresTableViewCell: UITableViewCell {
    
    @IBOutlet weak var highSchoolLabel: UILabel!
    @IBOutlet weak var satReadingScoreLabel: UILabel!
    @IBOutlet weak var satWritingScoreLabel: UILabel!
    @IBOutlet weak var satMathReadingScoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

