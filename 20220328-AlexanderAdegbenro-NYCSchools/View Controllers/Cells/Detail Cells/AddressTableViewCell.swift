import UIKit
import MapKit

class AddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressMapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func addHSAnnotaionWithCoordinates(_ hsCoordinates: CLLocationCoordinate2D){

        let highSchoolAnnotation = MKPointAnnotation()
        highSchoolAnnotation.coordinate = hsCoordinates
        self.addressMapView.addAnnotation(highSchoolAnnotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        let region = MKCoordinateRegion(center: highSchoolAnnotation.coordinate, span: span)
        let adjustRegion = self.addressMapView.regionThatFits(region)
        self.addressMapView.setRegion(adjustRegion, animated:true)
    }
}
