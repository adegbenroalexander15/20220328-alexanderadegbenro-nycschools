import UIKit

class NYCHSTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var ShareBtn: UIButton!
    @IBOutlet weak var AddressLbl: UILabel!
    @IBOutlet weak var PhoneNumBtn: UIButton!
    @IBOutlet weak var NavigateToAddressBtn: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCardViewShadows()
    }
    
    // MARK: Card View Customization Functions
    
    func setupCardViewShadows(){
        let view = cardView
        
        view?.layer.shadowRadius = 3
        view?.layer.shadowOpacity = 0.8
        view?.layer.cornerRadius = 15.0
        view?.layer.masksToBounds = false
        view?.layer.shadowColor = UIColor.black.cgColor
        view?.layer.shadowOffset = CGSize(width: 0, height: 2)
    }

}
