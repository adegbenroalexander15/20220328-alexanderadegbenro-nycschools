import UIKit

class NYCHighSchools: NSObject {

    var dbn: String?
    var schoolName: String?
    var schoolAddress: String?
    var schoolWebsite: String?
    var satMathAvgScore: String?
    var satWritinAvgScore: String?
    var overviewParagraph: String?
    var schoolTelephoneNumber: String?
    var satCriticalReadingAvgScore: String?
}
